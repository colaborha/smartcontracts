let FinnetToken = artifacts.require("FinnetToken");
const BigNumber = require('bignumber.js');
const assert = require('assert');


describe('Testing FinnetToken contracts', () => {
    contract("FinnetToken", function (accounts) {
        let finnetToken;

        before('setup contracts', async () => {
            finnetToken = await FinnetToken.new();
        });

        it("teste de totalSupply", function() {
            return finnetToken.totalSupply.call().then(function(result){
                assert.equal(result.toNumber(), 1000000000 * Math.pow(10,18), 'total supply is wrong');
            });
        });

        it("Teste de conta zerada", async function () {

            let n = await finnetToken.balanceOf.call(accounts[1]);

            assert.equal(
                n.toString(),
                new BigNumber(0).toString(),
                'Teste deve ser 0'
            );
        });


        it("Teste de transferencia 10 tokens: saldo 10", async function () {
            let x = await finnetToken.transfer(accounts[1], new BigNumber(10).toString());
            let n = await finnetToken.balanceOf.call(accounts[1]);

            assert.equal(
                n.toString(),
                new BigNumber(10).toString(),
                'Teste deve ser 10'
            );
        });

        it("Teste de transferencia 20 tokens: saldo 30", async function () {
            let x = await finnetToken.transfer(accounts[1], new BigNumber(20).toString());
            let n = await finnetToken.balanceOf.call(accounts[1]);

            assert.equal(
                n.toString(),
                new BigNumber(30).toString(),
                'Teste deve ser 30'
            );
        });

        it("Teste de transferencia 100 tokens: saldo 130", async function () {
            let x = await finnetToken.transfer(accounts[1], new BigNumber(100).toString());
            let n = await finnetToken.balanceOf.call(accounts[1]);

            assert.equal(
                n.toString(),
                new BigNumber(130).toString(),
                'Teste deve ser 130'
            );
        });


        it("Teste de exceção transferencia maior que supply", async function () {
            try {


                let x = await finnetToken.transfer(accounts[1], new BigNumber(1000000000 * Math.pow(10, 18)).toString());
                let n = await finnetToken.balanceOf.call(accounts[1]);


            } catch (err) {
                let b = err.message.startsWith("VM Exception while processing transaction: revert");

                assert.equal(
                    b,
                    true,
                    'Teste de catch'
                );
            }


        });


    });
});
