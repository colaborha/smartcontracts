const Ownable = artifacts.require('Ownable');


describe('Testing Ownable contracts', () => {
    contract('Ownable', function (accounts) {
        let ownable;
        before(async function () {
            ownable = await Ownable.new();
        });
        it('should have an owner', async function () {
            let owner = await ownable.owner();
            assert.isTrue(owner !== 0);
        });
        it('should have no changes', async function () {
            let changes = await ownable.getChanges();
            assert.isTrue(changes == 0);
        });
        it('changes owner after transfer', async function () {
            let other = accounts[1];
            await ownable.transferOwnership(other);
            let owner = await ownable.owner();
            assert.isTrue(owner === other);
        });
        it('should have a changes', async function () {
            let changes = await ownable.getChanges();
            assert.isTrue(changes == 1);
        });
    });
});