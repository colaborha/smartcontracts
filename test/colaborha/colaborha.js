let FinnetToken = artifacts.require("FinnetToken");
let ColaboRHa = artifacts.require("ColaboRHa");
const BigNumber = require('bignumber.js');
const assert = require('assert');


describe('Testing colaborha contracts', () => {
    contract("ColaboRHa", function (accounts) {
        let finnetToken;
        let colaboRHa;

        console.log(accounts);

        before('setup contracts', async () => {
            finnetToken = await FinnetToken.new();
            colaboRHa = await ColaboRHa.new(finnetToken.address);
        });

        it("Test Simula calculaDesempenho 50", async function () {
            let n = await colaboRHa.calculaDesempenho.call(
                50,   // Representa a participação do colaborador no desenvolvimento do projeto
                100,  // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Qualidade da atividade
                150,  // Representa a “nota” de Qualidade desempenhada em uma atividade.
                100,  // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Prazo (Tempo) da atividade
                0,    // Representa a “nota” de Prazo (Tempo) desempenhada em uma atividade
                200,  // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Custo da atividade
                75,   // Representa a “nota” de Custo desempenhada em uma atividade
                300,  // Representa um parâmetro para realização de média entre as “notas” de Qualidade, Prazo e Custo. O mesmo pode ser calibrado dependendo das intenções de uma empresa
                100,  // Representa um parâmetro opcional/complementar na realização do cálculo da bonificação
                10000 // Representa o valor cheio de tokens de recompensa ao ser finalizada uma atividade, ou seja, o valor total de sua bonificação caso a mesma possua um desempenho de 100% (sem falhas)
            );
            n /= new BigNumber(Math.pow(10,18));

            assert.equal(
                n.toString(),
                new BigNumber(50).toString(),
                'Teste deve ser 50'
            );
        });


        it("Test simula calculaDesempenho 75", async function () {
            let n = await colaboRHa.calculaDesempenho.call(
                50,   // Representa a participação do colaborador no desenvolvimento do projeto
                100,  // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Qualidade da atividade
                150,  // Representa a “nota” de Qualidade desempenhada em uma atividade.
                100,  // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Prazo (Tempo) da atividade
                150,  // Representa a “nota” de Prazo (Tempo) desempenhada em uma atividade
                100,  // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Custo da atividade
                150,  // Representa a “nota” de Custo desempenhada em uma atividade
                300,  // Representa um parâmetro para realização de média entre as “notas” de Qualidade, Prazo e Custo. O mesmo pode ser calibrado dependendo das intenções de uma empresa
                100,  // Representa um parâmetro opcional/complementar na realização do cálculo da bonificação
                10000 // Representa o valor cheio de tokens de recompensa ao ser finalizada uma atividade, ou seja, o valor total de sua bonificação caso a mesma possua um desempenho de 100% (sem falhas)
            );
            n /= new BigNumber(Math.pow(10,18));

            assert.equal(
                n.toString(),
                new BigNumber(75).toString(),
                'Teste deve ser 75'
            );
        });

    });
});
