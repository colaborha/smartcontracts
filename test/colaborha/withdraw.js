let FinnetToken = artifacts.require("FinnetToken");
let ColaboRHa = artifacts.require("ColaboRHa");
const BigNumber = require('bignumber.js');
const assert = require('assert');


describe('Testing Withdrawal contracts', () => {
    contract("ColaboRHa", function (accounts) {
        let finnetToken;
        let colaboRHa;
        let txAIS;

        // console.log(addresses);

        before('setup contracts', async () => {
            finnetToken = await FinnetToken.new();
            colaboRHa = await ColaboRHa.new(finnetToken.address);
            await finnetToken.transferOwnership(colaboRHa.address);
        });

        it("Test transferencia 50", async function () {
            txAIS = await colaboRHa.sendTokenFromAIS(
                50,   // Representa a participação do colaborador no desenvolvimento do projeto
                100,  // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Qualidade da atividade
                150,  // Representa a “nota” de Qualidade desempenhada em uma atividade.
                100,  // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Prazo (Tempo) da atividade
                0,    // Representa a “nota” de Prazo (Tempo) desempenhada em uma atividade
                200,  // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Custo da atividade
                75,   // Representa a “nota” de Custo desempenhada em uma atividade
                300,  // Representa um parâmetro para realização de média entre as “notas” de Qualidade, Prazo e Custo. O mesmo pode ser calibrado dependendo das intenções de uma empresa
                100,  // Representa um parâmetro opcional/complementar na realização do cálculo da bonificação
                10000, // Representa o valor cheio de tokens de recompensa ao ser finalizada uma atividade, ou seja, o valor total de sua bonificação caso a mesma possua um desempenho de 100% (sem falhas)
                accounts[1]
            );
            let x = await finnetToken.balanceOf.call(accounts[1]);
            x /= new BigNumber(Math.pow(10, 18));


            assert.equal(
                x.toString(),
                new BigNumber(50).toString(),
                'Teste deve ser 50'
            );
        });

        it("Deve ter 0 reserva", async function () {
            let y = await finnetToken.reservedOf.call(accounts[1]);


            assert.equal(
                y.toString(),
                new BigNumber(0).toString(),
                'Teste deve ser 0'
            );
        });


        it("Test reserva", async function () {
            await colaboRHa.reserve(
                accounts[1],
                50 * Math.pow(10, 18)
            );
            let x = await finnetToken.balanceOf.call(accounts[1]);
            let y = await finnetToken.reservedOf.call(accounts[1]);
            x /= new BigNumber(Math.pow(10, 18));

            assert.equal(
                x.toString(),
                new BigNumber(0).toString(),
                'Teste deve ser 0'
            );

            assert.equal(
                y.toString(),
                new BigNumber(50 * Math.pow(10, 18)).toString(),
                'Teste deve ser 50'
            );
        });

        it("Test withdraw", async function () {
            await colaboRHa.withdraw (
                accounts[1],
                50 * Math.pow(10, 18)
            );
            let x = await finnetToken.balanceOf.call(accounts[1]);
            let y = await finnetToken.reservedOf.call(accounts[1]);
            x /= new BigNumber(Math.pow(10, 18));

            assert.equal(
                x.toString(),
                new BigNumber(0).toString(),
                'Teste deve ser 0'
            );

            assert.equal(
                y.toString(),
                new BigNumber(0).toString(),
                'Teste deve ser 0'
            );
        });

        it("Test de exceção no Withdrawal", async function () {

            try {

                await colaboRHa.reserve(
                    accounts[1],
                    50 * Math.pow(10, 18)
                );
                let x = await finnetToken.balanceOf.call(accounts[1]);
                x /= new BigNumber(Math.pow(10, 18));

            } catch (err) {
                let b = err.message.startsWith("VM Exception while processing transaction: revert");

                assert.equal(
                    b,
                    true,
                    'Teste de catch'
                );
            }
        });

    });
});
