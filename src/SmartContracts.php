<?php
/**
 * --------------------------------------------------------------------------
 *
 * --------------------------------------------------------------------------
 * PHP version X.X
 *
 *
 * @category ColaboRHa
 * @package  ColaboRHa
 * @author   Ercy Moreira Neto <ercy.neto@finnet.com.br>
 * @since    X.X.X
 * @license  Property <http://www.finnet.com.br>
 * @link     http://www.finnet.com.br
 * @created  01/10/2018 14:12
 * @updated  01/10/2018 14:12
 */

namespace Finnet\Colaborha;

/**
 * Class SmartContracts
 *
 * @package Finnet\Colaborha
 */
class SmartContracts
{
    /**
     * @param string $contract_name Nome do SmartContract a ser retornado
     * @param bool   $base64        Se o código do contrato retorna em base64
     *
     * @return array
     * @throws \Exception
     */
    public static function getContract($contract_name, $base64 = true)
    {
        $file_name = "{$contract_name}.sol";
        $path_file = __DIR__ . "../contracts/{$file_name}";
        if (!file_exists($path_file)) {
            throw new \Exception("SmartContract não encontrado.");
        }

        $content = file_get_contents($path_file);

        if ($base64) {
            $content = base64_encode($content);
        }

        return [
            'filename'      => "{$file_name}:{$contract_name}",
            'contract_code' => $content,
        ];
    }

    /**
     * @throws \Exception
     */
    public static function getAllContracts()
    {
        $contracts = [];
        foreach (glob("../contracts/*.sol") as $filename) {
            $arquivo = explode(".", basename($filename));
            $contrato = $arquivo[0];
            $contracts[] = self::getContract($contrato);
        }

        return $contracts;
    }
}