pragma solidity ^0.4.24;

import './Ownable.sol';
import './ERC20Interface.sol';
import './Refundable.sol';
import './Withdrawable.sol';

contract FinnetToken is ERC20Interface, Ownable, Refundable, Withdrawable
{
    string public symbol;
    string public  name;
    uint8 public decimals;
    uint256 public totalSupply;

    constructor() public payable
    {
        symbol = "FNT";
        name = "Finnet Token";
        decimals = 18;
        totalSupply = 1000000000 * 10**uint(decimals);
        balances[msg.sender] = totalSupply;
        emit Transfer(address(0), msg.sender, totalSupply);
    }

    function transfer(address _to, uint256 _value) public payable returns (bool success)
    {
        //Default assumes totalSupply can't be over max (2^256 - 1).
        //If your token leaves out totalSupply and can issue more tokens as time goes on, you need to check if it doesn't wrap.
        //Replace the if with this one instead.
        //if (balances[msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
        require(balances[msg.sender] >= _value && _value > 0);

        if (balances[msg.sender] >= _value && _value > 0) {
            balances[msg.sender] -= _value;
            balances[_to] += _value;
            emit Transfer(msg.sender, _to, _value);
            return true;
        } else { return false; }
    }

    function ownerTransfer(address _from, address _to, uint256 _value) onlyOwner private  returns (bool success)
    {
        require(balances[_from] >= _value  && _value > 0 );


        if (balances[_from] >= _value  && _value > 0) {
            balances[_to] += _value;
            balances[_from] -= _value;
            emit Transfer(_from, _to, _value);
            return true;
        } else { return false; }
    }

    function reserveToken(address _from, uint256 _value) onlyOwner private  returns (bool success)
    {
        require(balances[_from] >= _value  && _value > 0 );

        if (balances[_from] >= _value  && _value > 0) {
            reserved[_from] += _value;
            balances[_from] -= _value;
            emit Reserve(_from, _value);
            return true;
        } else { return false; }
    }

    function withdrawReserve(address _from, address _to, uint256 _value) onlyOwner private  returns (bool success)
    {
        require(reserved[_from] >= _value  && _value > 0 );

        if (reserved[_from] >= _value  && _value > 0) {
            balances[_to] += _value;
            reserved[_from] -= _value;
            emit Transfer(_from, _to, _value);
            return true;
        } else { return false; }
    }

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success)
    {
        //same as above. Replace this line with the following if you want to protect against wrapping uints.
        //if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && balances[_to] + _value > balances[_to]) {

        require((balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) );


        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) {
            balances[_to] += _value;
            balances[_from] -= _value;
            allowed[_from][msg.sender] -= _value;
            emit Transfer(_from, _to, _value);
            return true;
        } else { return false; }
    }

    function balanceOf(address _owner) public returns (uint256 retBalance)
    {
        return balances[_owner];
    }

    function reservedOf(address _owner) public returns (uint256 retBalance)
    {
        return reserved[_owner];
    }



    function approve(address _spender, uint256 _value) public returns (bool success)
    {
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) public returns (uint256 remaining)
    {
        return allowed[_owner][_spender];
    }

    function transferOwnership(address _owner) onlyOwner payable public
    {
        if(msg.sender != _owner) {

            balances[_owner] += balances[msg.sender];
            balances[msg.sender] = 0;

            super.transferOwnership(_owner);
        }
    }

    function getDecimal() view public returns ( uint8 dec )
    {
        return decimals;
    }

    mapping (address => uint256) balances;
    mapping (address => uint256) reserved;
    mapping (address => mapping (address => uint256)) allowed;
}