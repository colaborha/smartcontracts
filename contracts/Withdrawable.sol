pragma solidity ^0.4.24;

import './FinnetToken.sol';
import './Ownable.sol';
import './ERC20Interface.sol';

contract Withdrawable

{
    address public owner;

    mapping (address => uint256) reserves;
    mapping (address => uint256) balances;

    function reserveToken(address _from, uint256 _value) private  returns (bool success);
    function withdrawReserve(address _from, address _to, uint256 _value) private  returns (bool success);
    function reservedOf(address _owner) public returns (uint256 retBalance);

    event Transfer(address indexed from, address indexed to, uint256 tokens);
    event Reserve(address indexed from, uint256 tokens);
    event Withdraw(address indexed from, uint256 tokens);

    function reserve(address _from, uint256 _value) public payable returns (bool success)
    {
        if ( reserveToken(_from, _value) ){
            emit Reserve(_from, _value);
            return true;
        } else {
            return false;
        }
    }

    function withdraw(address _from, uint256 _value) public payable returns (bool success)
    {
        if ( withdrawReserve(_from, msg.sender, _value) ){
            emit Withdraw(_from, _value);
            return true;
        } else {
            return false;
        }
    }



}
