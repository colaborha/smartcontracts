pragma solidity ^0.4.24;

import './Ownable.sol';

contract Refundable is Ownable
{
    event Refund(address indexed from, address indexed to, uint256 tokens);

    function ownerTransfer(address _from, address _to, uint256 _value) private  returns (bool success);

    function refund(address _from, uint256 _value) public payable returns (bool success)
    {
        if ( ownerTransfer(_from, msg.sender, _value) ){
            emit Refund(_from,  msg.sender, _value);
            return true;
        } else {
            return false;
        }

    }
}
