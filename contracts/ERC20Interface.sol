pragma solidity ^0.4.24;

contract ERC20Interface
{
    uint256 public totalSupply;
    function balanceOf(address _owner) public returns (uint256 retBalance);
    function allowance(address _owner, address _spender) public returns (uint256 remaining);
    function transfer(address _to, uint256 _value) public payable returns (bool success);
    function approve(address _spender, uint256 _value) public returns (bool success);
    function transferFrom(address _from, address _to, uint256 _value) public  returns (bool success);

    event Transfer(address indexed from, address indexed to, uint256 tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint256 tokens);
}
