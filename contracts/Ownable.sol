pragma solidity ^0.4.0;

contract Ownable
{
    address public owner;
    address public origin;
    uint256 changes_;

    constructor() public
    {
        owner = msg.sender;
        changes_ = 0;
    }

    modifier onlyOwner
    {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address newOwner) onlyOwner payable public
    {
        owner = newOwner;
        changes_ = changes_ + 1;
    }

    function getChanges() public view returns (uint256) {
        return changes_;
    }
}
