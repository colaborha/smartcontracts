pragma solidity ^0.4.24;

// import './FinnetToken.sol';



contract FinnetTokenInterface
{
    function transfer(address _to, uint256 _value) public payable returns (bool success);
    function ownerTransfer(address _from, address _to, uint256 _value)  private  returns (bool success);
    function reserveToken(address _from, uint256 _value)  private  returns (bool success);
    function withdrawReserve(address _from, address _to, uint256 _value)  private  returns (bool success);
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success);
    function balanceOf(address _owner) public returns (uint256 retBalance);
    function reservedOf(address _owner) public returns (uint256 retBalance);
    function approve(address _spender, uint256 _value) public returns (bool success);

    function allowance(address _owner, address _spender) public returns (uint256 remaining);

    function transferOwnership(address _owner) payable public;

    function refund(address _from, uint256 _value) public payable returns (bool success);
    function reserve(address _from, uint256 _value) public payable returns (bool success);
    function withdraw(address _from, uint256 _value) public payable returns (bool success) ;


    function getDecimal() view public returns ( uint8 dec ) ;


}

contract ColaboRHa
{
    FinnetTokenInterface fntToken;

    address public ownerFinnet;
    address public tokenFinnet;
    uint8 public decimals;

    constructor(address _token) public
    {
        require(_token !=0);
        fntToken = FinnetTokenInterface(_token);

        decimals = fntToken.getDecimal();
        tokenFinnet = _token;

        ownerFinnet = msg.sender;
    }

    function refund( uint256 _value,address _from, bytes32  _tx_refund) public payable returns (bool success)
    {
        _tx_refund = _tx_refund;

        if(fntToken.refund(_from, _value)) {
            emit Transfer(_from, msg.sender, _value);
            return true;
        } else {
            return false;
        }
    }

    function reserve( address _from, uint256 _value) public payable returns (bool success)
    {

        if(fntToken.reserve(_from, _value)) {
            emit Transfer(_from, msg.sender, _value);
            return true;
        } else {
            return false;
        }
    }

    function withdraw( address _from, uint256 _value) public payable returns (bool success)
    {
        if(fntToken.withdraw(_from, _value)) {
            emit Transfer(_from, msg.sender, _value);
            return true;
        } else {
            return false;
        }
    }

    function sendTokenFromAIS(
        uint256 _pariticpacao,    // Representa a participação do colaborador no desenvolvimento do projeto
        uint256 _rel_qualidade,   // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Qualidade da atividade
        uint256 _nota_qualidade,  // Representa a “nota” de Qualidade desempenhada em uma atividade.
        uint256 _rel_prazo,       // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Prazo (Tempo) da atividade
        uint256 _nota_prazo,      // Representa a “nota” de Prazo (Tempo) desempenhada em uma atividade
        uint256 _rel_custo,       // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Custo da atividade
        uint256 _nota_custo,      // Representa a “nota” de Custo desempenhada em uma atividade
        uint256 _parametro_media, // Representa um parâmetro para realização de média entre as “notas” de Qualidade, Prazo e Custo. O mesmo pode ser calibrado dependendo das intenções de uma empresa
        uint256 _complemento,     // Representa um parâmetro opcional/complementar na realização do cálculo da bonificação
        uint256 _valor_total,     // Representa o valor cheio de tokens de recompensa ao ser finalizada uma atividade, ou seja, o valor total de sua bonificação caso a mesma possua um desempenho de 100% (sem falhas)
        address _to               // Endereço da carteira destino
    ) public payable returns (bool success)
    {
        uint256 _value = calculaDesempenho(
            _pariticpacao ,
            _rel_qualidade,
            _nota_qualidade,
            _rel_prazo,
            _nota_prazo,
            _rel_custo,
            _nota_custo,
            _parametro_media,
            _complemento,
            _valor_total
        );


        if(fntToken.transfer(_to, _value)) {
            emit Transfer(msg.sender, _to, _value);
            return true;
        } else {
            return false;
        }
    }

    function restoreOwnership() onlyOwner public
    {
        fntToken.transferOwnership(ownerFinnet);
    }

    modifier onlyOwner
    {
        require(msg.sender == ownerFinnet);
        _;
    }


    function calculaDesempenho(
        uint256 _pariticpacao ,   // Representa a participação do colaborador no desenvolvimento do projeto
        uint256 _rel_qualidade,   // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Qualidade da atividade
        uint256 _nota_qualidade,  // Representa a “nota” de Qualidade desempenhada em uma atividade.
        uint256 _rel_prazo,       // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Prazo (Tempo) da atividade
        uint256 _nota_prazo,      // Representa a “nota” de Prazo (Tempo) desempenhada em uma atividade
        uint256 _rel_custo,       // Representa um parâmetro de calibração para a relevância que será dada ao parâmetro de “nota” de Custo da atividade
        uint256 _nota_custo,      // Representa a “nota” de Custo desempenhada em uma atividade
        uint256 _parametro_media, // Representa um parâmetro para realização de média entre as “notas” de Qualidade, Prazo e Custo. O mesmo pode ser calibrado dependendo das intenções de uma empresa
        uint256 _complemento,     // Representa um parâmetro opcional/complementar na realização do cálculo da bonificação
        uint256 _valor_total      // Representa o valor cheio de tokens de recompensa ao ser finalizada uma atividade, ou seja, o valor total de sua bonificação caso a mesma possua um desempenho de 100% (sem falhas)
    ) view public returns (uint _value)
    {

        _value = _pariticpacao * ( (_rel_qualidade * _nota_qualidade) + (_rel_prazo * _nota_prazo) + (_rel_custo * _nota_custo)  );
        _value = ( _value / _parametro_media ) * _complemento * _valor_total;
        _value = _value * 10**uint(decimals-8);

        _value = _value;

        return _value;
    }

    event Transfer(address indexed from, address indexed to, uint256 tokens);
}